fetch('http://api.openweathermap.org/data/2.5/weather?id=625144&appid=b209cf3f7a8a1a8eb52c349ebc062c8b')
    .then(function (resp) {return resp.json() })
    .then(function (data) {
        console.log(data);
        document.querySelector('.city').innerHTML = data.name;
        document.querySelector('.temp').innerHTML = Math.round(data.main.temp - 273) + '&deg';
        document.querySelector('.weather').innerHTML = data.weather[0]['description'];
        document.querySelector('.feature').src = `https://openweathermap.org/img/wn/${data.weather[0]['icon']}@2x.png`;
    })
    .catch(function () {
    });